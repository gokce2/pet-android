package com.gokce.hayvantakip.api;

import com.gokce.hayvantakip.api.domain.User;
import com.gokce.hayvantakip.api.util.LoginRequest;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    @POST("customers/login")
    Single<User> login(@Body LoginRequest request);
}
