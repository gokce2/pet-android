package com.gokce.hayvantakip.api;

import com.gokce.hayvantakip.api.util.SettingsStore;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;

@Singleton
public class Api {
    private final AppService appService;
    private final SettingsStore settingsStore;

    @Inject
    public Api(@NonNull AppService appService,
               @NonNull SettingsStore settingsStore) {
        this.appService = appService;
        this.settingsStore = settingsStore;
    }


}
