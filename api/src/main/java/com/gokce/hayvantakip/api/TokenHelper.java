package com.gokce.hayvantakip.api;

import android.util.Log;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.api.domain.User;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class TokenHelper {
    private static final String TAG = TokenHelper.class.getName();

    private String token;

    @Inject
    public TokenHelper() {

    }

    public String getToken() {
        return token;
    }

    public void setToken(@NonNull String token) {
        this.token = "Bearer " + token;
    }

    public void afterLogin(@NonNull User user) {
        if (user.getAccessToken() == null) {
            log("voiding auth");
            voidAuth();
            return;
        }
        this.token = "Bearer " + user.getAccessToken();
    }

    private void log(@NonNull String message) {
        Log.d(TAG, message);
    }

    public void voidAuth() {
        this.token = null;
    }
}
