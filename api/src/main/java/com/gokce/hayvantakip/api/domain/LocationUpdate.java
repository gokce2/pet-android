package com.gokce.hayvantakip.api.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LocationUpdate implements Serializable {
    private long          device;
    private double        lat;
    private double        lng;
    private Device.Status status;

}
