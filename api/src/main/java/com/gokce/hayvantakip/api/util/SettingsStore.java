package com.gokce.hayvantakip.api.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@Singleton
public class SettingsStore {
    private final SharedPreferences preferences;
    private Settings settings;

    @Inject
    SettingsStore(@NonNull Context context) {
        preferences = context.getSharedPreferences("app_settings", Context.MODE_PRIVATE);
    }

    public void store(@NonNull Settings settings) {
        this.settings = settings;
        preferences.edit()
                .putString("settings", settings.toString())
                .apply();
    }

    @Nullable
    public Settings get() {
        if (settings != null) {
            return settings;
        }
        String json = preferences.getString("settings", null);
        if (json == null) {
            return null;
        }
        this.settings = new Gson().fromJson(json, Settings.class);
        return settings;
    }

    public void clear() {
        settings = null;
        preferences.edit().clear().apply();
    }

}
