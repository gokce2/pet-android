package com.gokce.hayvantakip.api;

import com.gokce.hayvantakip.api.domain.Device;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface AppService {
    @GET("customers/devices")
    Single<List<Device>> devices();
}
