package com.gokce.hayvantakip.api.domain;

import com.gokce.hayvantakip.api.util.LatLng;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Device implements Serializable {
    private long   id;
    private String name;
    private User   user;
    private LatLng location;
    private Status status;

    public boolean isOffline() {
        return status == Status.STOPPED;
    }

    public enum Status {
        ACTIVE, STOPPED, PAUSED
    }
}
