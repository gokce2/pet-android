package com.gokce.hayvantakip.api.util;


import com.google.gson.Gson;

import java.io.Serializable;

import androidx.annotation.NonNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
@Setter
@Builder
@AllArgsConstructor
public final class Settings implements Serializable {
    private String url;
    private String deviceId;
    private String devicePassword;
    private String userId;
    private String userPassword;

    @NonNull
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
