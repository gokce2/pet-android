package com.gokce.hayvantakip.api.util.exception;

public class InvalidPasswordException extends Exception {
    public InvalidPasswordException() {
        super("wrong password");
    }
}
