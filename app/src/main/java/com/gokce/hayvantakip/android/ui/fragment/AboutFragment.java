package com.gokce.hayvantakip.android.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.base.BaseFragment;
import com.gokce.hayvantakip.android.databinding.FragmentAboutUsBinding;


public class AboutFragment extends BaseFragment {
    private final String about =
            "I HATE EVERYTING ABOUT ENGINNERING. I mean I love literature.";

    private FragmentAboutUsBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAboutUsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.aboutTextView.setText(about);
    }
}
