package com.gokce.hayvantakip.android.util;

import androidx.annotation.StringRes;

import com.gokce.hayvantakip.android.App;


public class ResThrowable extends Throwable {
    public ResThrowable(@StringRes int stringRes, Object... args) {
        super(App.getInstance().getString(stringRes, args));
    }
}
