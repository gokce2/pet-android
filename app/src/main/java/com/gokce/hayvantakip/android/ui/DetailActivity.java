package com.gokce.hayvantakip.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.base.BaseToolbarActivity;
import com.gokce.hayvantakip.android.ui.fragment.AboutFragment;
import com.gokce.hayvantakip.android.ui.more.MoreMenuItem;


public class DetailActivity extends BaseToolbarActivity {

    private static final String TAG = DetailActivity.class.getSimpleName();


    public static void show(@NonNull Context context,
                            @NonNull MoreMenuItem.ID action) {
        context.startActivity(new Intent(context, DetailActivity.class)
                .putExtra("action", action));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        MoreMenuItem.ID action = (MoreMenuItem.ID) getIntent().getSerializableExtra("action");
        if (action != null) {
            handleAction(action);
            return;
        }

        Log.e(TAG, "onCreate: no action provided, killing myself ");
        finish();
    }

    private void handleAction(@NonNull MoreMenuItem.ID action) {
        switch (action) {
            case ABOUT:
                setTitle(R.string.about_us);
                showFragment(new AboutFragment());
                break;
        }
    }

    private void showFragment(@NonNull Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        transaction.replace(R.id.fragmentFrame, fragment, "detail")
                .commit();
    }
}
