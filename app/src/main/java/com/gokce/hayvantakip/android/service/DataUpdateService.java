package com.gokce.hayvantakip.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.util.Prefs;
import com.gokce.hayvantakip.api.AppService;
import com.gokce.hayvantakip.api.domain.Device;
import com.gokce.hayvantakip.api.domain.LocationUpdate;
import com.gokce.hayvantakip.api.domain.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DataUpdateService extends Service {
    private static final String TAG = DataUpdateService.class.getSimpleName();
    @Inject
    Prefs      prefs;
    @Inject
    AppService appService;
    private CompositeDisposable disposable;
    private DatabaseReference   reference;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
        reference = FirebaseDatabase.getInstance().getReference().child("data");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        prefs.getUser().ifPresent(this::listen);
        return START_NOT_STICKY;
    }

    private void listen(@NonNull User user) {
        if (disposable != null) {
            disposable.dispose();
        }
        disposable = new CompositeDisposable();
        Disposable d = appService.devices()
                .subscribeOn(Schedulers.io())
                .subscribe(devices -> {
                    for (Device device : devices) {
                        listen(device);
                    }
                }, error -> {
                    Log.w(TAG, "listen: i will kill you" + error.getLocalizedMessage());
                });
        disposable.add(d);

    }

    private void listen(@NonNull Device device) {
        Disposable d = RxFirebaseDatabase.observeValueEvent(reference
                .child(String.valueOf(device.getUser().getId())).child(String.valueOf(device.getId())))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(dataSnapshot -> {
                    if (dataSnapshot.exists()) {
                        LocationUpdate locationUpdate = dataSnapshot.getValue(LocationUpdate.class);
                        if (locationUpdate != null) {
                            Log.d(TAG, "listen: Sending location update for device " + locationUpdate.getDevice());
                            EventBus.getDefault().post(locationUpdate);
                        } else {
                            Log.e(TAG, "listen: location update is null");
                        }
                    } else {
                        Log.e(TAG, "listen: Data deleted from firebase");
                    }
                }, error -> {
                    Log.e(TAG, "listen: Failed to subscribe because " + error.getLocalizedMessage());
                });
        disposable.add(d);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}
