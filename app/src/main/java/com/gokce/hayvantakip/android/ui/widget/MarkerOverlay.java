package com.gokce.hayvantakip.android.ui.widget;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.helper.Helper;

import org.osmdroid.api.IMapView;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

import java8.util.Optional;

public class MarkerOverlay extends ItemizedOverlay<OverlayItem> {
    private final float                  textSize;
    private       ArrayList<OverlayItem> overlayItems = new ArrayList<>();
    private       OnMarkerClick          onMarkerClick;
    private       boolean                showLabel    = true;

    public MarkerOverlay(Drawable pDefaultMarker) {
        super(pDefaultMarker);
        textSize = Helper.pxFromDp(12);
    }

    public boolean isShowLabel() {
        return showLabel;
    }

    public void setShowLabel(boolean showLabel) {
        this.showLabel = showLabel;
    }

    public void addOverlayItem(@NonNull OverlayItem item) {
        overlayItems.add(item);
        populate();
    }


    public void updateOverlayItem(@NonNull OverlayItem item) {
        int index = findIndex(item);
        if (index == -1) {
            return;
        }
        overlayItems.set(index, item);
        populate();
    }

    private int findIndex(@NonNull OverlayItem item) {
        int index = -1;
        for (OverlayItem overlayItem : overlayItems) {
            if (overlayItem.getTitle().equals(item.getTitle())) {
                return ++index;
            }
            index++;
        }
        return -1;
    }

    public void removeAll() {
        overlayItems.clear();
        populate();
    }

    public void setOnMarkerClick(@Nullable OnMarkerClick onMarkerClick) {
        this.onMarkerClick = onMarkerClick;
    }

    @Override
    protected boolean onTap(int index) {
        if (onMarkerClick != null) {
            onMarkerClick.onMarkerClick(overlayItems.get(index).getUid());
            return true;
        }
        return super.onTap(index);
    }


    public Optional<OverlayItem> findItem(@NonNull String uId) {
        for (OverlayItem item : overlayItems) {
            if (uId.equals(item.getUid())) {
                return Optional.of(item);
            }
        }
        return Optional.empty();
    }

    @Override
    protected OverlayItem createItem(int i) {
        return overlayItems.get(i);
    }

    @Override
    public int size() {
        return overlayItems.size();
    }

    @Override
    public boolean onSnapToItem(int x, int y, Point snapPoint, IMapView mapView) {
        return false;
    }

    @Override
    public void draw(android.graphics.Canvas canvas, MapView mapView, boolean shadow) {
        super.draw(canvas, mapView, shadow);
        if (showLabel) {
            if (!shadow) {
                for (int index = 0; index < overlayItems.size(); index++) {
                    OverlayItem item          = overlayItems.get(index);
                    GeoPoint    point         = (GeoPoint) item.getPoint();
                    Point       ptScreenCoord = new Point();
                    mapView.getProjection().toPixels(point, ptScreenCoord);
                    Paint paint = new Paint();
                    paint.setShadowLayer(20, 0, 0, Color.BLACK);
                    paint.setTypeface(Typeface.DEFAULT_BOLD);
                    paint.setTextAlign(Paint.Align.CENTER);
                    paint.setTextSize(textSize);
                    paint.setARGB(150, 0, 0, 0);
                    canvas.drawText(item.getTitle(), ptScreenCoord.x, ptScreenCoord.y + textSize, paint);
                }
            }
        }
    }

    public interface OnMarkerClick {
        void onMarkerClick(@NonNull String imei);
    }
}
