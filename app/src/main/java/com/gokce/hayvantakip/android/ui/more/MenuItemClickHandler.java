package com.gokce.hayvantakip.android.ui.more;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.ui.DetailActivity;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

class MenuItemClickHandler implements MoreAdapter.OnMenuItemClick {
    private final MoreFragment fragment;

    public MenuItemClickHandler(MoreFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onClick(@NonNull MoreMenuItem menuItem) {
        switch (menuItem.getId()) {
            case LOGOUT:
                performLogout();
                break;
            case ABOUT:
                openDetail(menuItem.getId());
                break;
            default:
                System.out.println("Unhandled click for " + menuItem.getId());
        }
    }

    private void openDetail(@NonNull MoreMenuItem.ID id) {
        fragment.context()
                .ifPresent(context -> {
                    DetailActivity.show(context, id);
                });
    }


    private void performLogout() {
        if (fragment.getContext() != null) {
            new MaterialAlertDialogBuilder(fragment.getContext())
                    .setPositiveButton(R.string.yes,
                            (dialog, which) -> fragment.logout())
                    .setNegativeButton(R.string.no, null)
                    .setTitle(R.string.logout)
                    .setMessage(R.string.logout_message)
                    .show();
        }
    }
}
