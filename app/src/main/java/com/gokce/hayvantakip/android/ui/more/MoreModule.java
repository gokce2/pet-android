package com.gokce.hayvantakip.android.ui.more;


import com.gokce.hayvantakip.android.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MoreModule {
    @Provides
    @Singleton
    static List<MoreMenuItem> menuItems() {
        List<MoreMenuItem> items = new ArrayList<>();
        items.add(MoreMenuItem.builder()
                .title(R.string.notifications)
                .subtitle(R.string.subtitle_notifications)
                .id(MoreMenuItem.ID.NOTIFICATIONS)
                .icon(R.drawable.ic_notifications_black_24dp)
                .build());
        items.add(MoreMenuItem.builder()
                .title(R.string.about_us)
                .subtitle(R.string.made_by_message)
                .id(MoreMenuItem.ID.ABOUT)
                .icon(R.drawable.ic_info_black_24dp)
                .build());
        items.add(MoreMenuItem.builder()
                .icon(R.drawable.ic_logout)
                .title(R.string.logout)
                .hideNext(true)
                .id(MoreMenuItem.ID.LOGOUT)
                .subtitle(R.string.logout_subtitle)
                .build());
        return items;
    }

}
