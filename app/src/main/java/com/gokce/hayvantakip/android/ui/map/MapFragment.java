package com.gokce.hayvantakip.android.ui.map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.base.BaseFragment;
import com.gokce.hayvantakip.android.databinding.FragmentMapBinding;
import com.gokce.hayvantakip.android.helper.Helper;
import com.gokce.hayvantakip.android.ui.widget.MarkerOverlay;
import com.gokce.hayvantakip.android.util.DialogUtil;
import com.gokce.hayvantakip.android.util.ResThrowable;
import com.gokce.hayvantakip.api.AppService;
import com.gokce.hayvantakip.api.domain.Device;
import com.gokce.hayvantakip.api.domain.LocationUpdate;
import com.gokce.hayvantakip.api.util.LatLng;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java8.util.stream.StreamSupport;

;

public class MapFragment extends BaseFragment {
    private static final String TAG = MapFragment.class.getSimpleName();
    @Inject
    AppService appService;
    private FragmentMapBinding bind;
    private IMapController     mapController;
    private MarkerOverlay      markerOverlay;
    private boolean            showLabel = true;
    private Drawable           blackDrawable;
    private Drawable           yellowDrawable;
    private Drawable           greenDrawable;
    private Drawable           redDrawable;

    private List<Device> devices;

    public static MapFragment newInstance() {
        Bundle      args     = new Bundle();
        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bind = FragmentMapBinding.inflate(inflater, container, false);
        return bind.getRoot();
    }

    private Disposable disposable;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fixMap();
        disposable = appService.devices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(devices -> {
                    this.devices = devices;
                    addMarkers(devices);
                }, this::showError);
    }

    private void fixMap() {
        bind.mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapController = bind.mapView.getController();
        mapController.setZoom(14.0);
        bind.mapView.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.NEVER);
        bind.mapView.setMultiTouchControls(true);

    }

    private void addMarkers(@NonNull List<Device> cars) {
        if (cars.size() == 0) {
            showError(new ResThrowable(R.string.no_devices));
            return;
        }
        if (markerOverlay == null) {
            Drawable drawable = context.getDrawable(R.drawable.ic_pets_black_24dp);
            markerOverlay = new MarkerOverlay(drawable);
            markerOverlay.setShowLabel(showLabel);
            GeoPoint point = new GeoPoint(cars.get(0).getLocation().getLat(), cars.get(0).getLocation().getLng());
            mapController.setCenter(point);
        } else {
            bind.mapView.getOverlayManager().remove(markerOverlay);
            markerOverlay.removeAll();
        }
        StreamSupport.stream(cars)
                .forEach(car -> {
                    OverlayItem item = new OverlayItem(car.getName(),
                            car.getName(),
                            car.getName(), new GeoPoint(car.getLocation().getLat(), car.getLocation().getLng()));
                    item.setMarker(markerIcon(car));
                    markerOverlay.addOverlayItem(item);
                });
        bind.mapView.getOverlayManager().add(markerOverlay);
        bind.mapView.invalidate();
        updatePlateChips(cars);
    }

    private void showError(@NonNull Throwable throwable) {
        if (!isDetached()) {
            activity().ifPresent(a -> {
                DialogUtil.showOk(a, throwable.getLocalizedMessage());
            });
        }
    }

    private void updatePlateChips(@NonNull List<Device> cars) {
        bind.plateChips.removeAllViews();
        StreamSupport.stream(cars)
                .forEach(car -> bind.plateChips.addView(createPlateChip(car)));
    }

    private Chip createPlateChip(@NonNull Device car) {
        Chip chip = new Chip(context);
        chip.setChipBackgroundColorResource(R.color.veryLightPrimaryColor);
        chip.setTag(car.getName());
        chip.setText(car.getName());
        chip.setOnClickListener(v -> {
            if (car.getLocation() == null) {
                showNoRecord(car);
            } else {
                markerOverlay.findItem(car.getName())
                        .ifPresent(overlayItem -> {
                            double zoomLevel = bind.mapView.getZoomLevelDouble() > 18 ? bind.mapView.getZoomLevelDouble() : 18;
                            mapController.animateTo(overlayItem.getPoint(), zoomLevel, 500L);
                        });
            }
        });
        return chip;
    }

    private void showNoRecord(@NonNull Device vehicle) {
        new MaterialAlertDialogBuilder(context)
                .setTitle(vehicle.getName())
                .setMessage(R.string.no_record_found)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        bind.mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        bind.mapView.onPause();
    }


    private Drawable getDrawable(@DrawableRes int res) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), res);
        int    px     = (int) Helper.pxFromDp(30);
        int    height = (int) Helper.pxFromDp(33);
        return new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap,
                px, height, true));
    }

    private Drawable markerIcon(@NonNull Device hayvan) {
        Drawable drawable = null;
        if (hayvan.isOffline()) {
            if (blackDrawable == null) {
                blackDrawable = getDrawable(R.drawable.marker_black);
            }
            drawable = blackDrawable;
        } else if (hayvan.getStatus() != null) {
            if (hayvan.getStatus() == Device.Status.PAUSED) {
                if (yellowDrawable == null) {
                    yellowDrawable = getDrawable(R.drawable.marker_yellow);
                }
                drawable = yellowDrawable;
            } else if (hayvan.getStatus() == Device.Status.ACTIVE) {
                if (greenDrawable == null) {
                    greenDrawable = getDrawable(R.drawable.marker_green);
                }
                drawable = greenDrawable;
            }
        }
        return drawable;
    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void updateSingle(@NonNull LocationUpdate single) {
        if (isDetached()) {
            return;
        }
        findDevice(single, device -> {
            int index = devices.indexOf(device);
            if (index == -1) {
                return;
            }
            devices.remove(index);
            device.setLocation(new LatLng(single.getLat(), single.getLng()));
            devices.add(0, device);
            if (device.getName() != null) {
                markerOverlay.findItem(device.getName())
                        .ifPresent(item -> {
                            OverlayItem newItem = new OverlayItem(device.getName(),
                                    device.getName(),
                                    device.getName(), new GeoPoint(device.getLocation().getLat(), device.getLocation().getLng()));
                            newItem.setMarker(markerIcon(device));
                            markerOverlay.updateOverlayItem(newItem);
                            if (devices.size() == 1) {
                                double zoomLevel = bind.mapView.getZoomLevelDouble() > 18 ? bind.mapView.getZoomLevelDouble() : 18;
                                mapController.animateTo(newItem.getPoint(), zoomLevel, 500L);
                            }
                            bind.mapView.invalidate();
                            Log.i(TAG, "updateSingle: updated " + device.getName());
                        });
            }
        });

    }

    private void findDevice(@NonNull LocationUpdate update, @NonNull Consumer<Device> consumer) {
        if (this.devices == null) {
            return;
        }
        for (Device device : devices) {
            if (device.getId() == update.getDevice()) {
                consumer.accept(device);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();

    }
}

