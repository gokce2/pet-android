package com.gokce.hayvantakip.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.base.BaseActivity;
import com.gokce.hayvantakip.android.databinding.ActivityMainBinding;
import com.gokce.hayvantakip.android.service.DataUpdateService;
import com.gokce.hayvantakip.android.ui.map.MapFragment;
import com.gokce.hayvantakip.android.ui.more.MoreFragment;
import com.gokce.hayvantakip.api.TokenHelper;

import org.osmdroid.config.Configuration;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {
    private ActivityMainBinding binding;
    private Fragment            lastFragment;

    @Inject
    TokenHelper tokenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs.getUser().ifPresent(tokenHelper::afterLogin);
        Configuration.getInstance().setOsmdroidBasePath(getCacheDir());
        Configuration.getInstance().setUserAgentValue("hayvan-takip");
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        startService(new Intent(this, DataUpdateService.class));
        String tag = null;
        if (savedInstanceState != null) {
            tag = savedInstanceState.getString("tag");
            if (tag != null) {
                lastFragment = getSupportFragmentManager().findFragmentByTag(tag);
            }
        }
        if (tag == null) {
            tag = "home";
        }
        binding.bottomNav.setOnNavigationItemSelectedListener(this::onNavItemSelected);
        showFragment(tag);
    }

    private boolean onNavItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.itemMap:
                showFragment("home");
                break;
            case R.id.itemMore:
                showFragment("more");
                break;

        }
        return true;
    }

    private Fragment createFragment(@NonNull String forTag) {
        switch (forTag) {
            case "home":
                return MapFragment.newInstance();
            case "more":
                return new MoreFragment();
            default:
                return new Fragment();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (lastFragment != null) {
            String tag = lastFragment.getTag();
            outState.putString("tag", tag);
        }
        super.onSaveInstanceState(outState);
    }

    private void showFragment(@NonNull String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            fragment = createFragment(tag);
        }
        showFragment(fragment, tag);
    }

    private void showFragment(@NonNull Fragment fragment,
                              @NonNull String tag) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (getSupportFragmentManager().findFragmentByTag(tag) == null) {
            transaction.add(R.id.fragmentFrame, fragment, tag);
        }
        if (lastFragment != null && !tag.equals(lastFragment.getTag())) {
            transaction.hide(lastFragment);
        }
        transaction.show(fragment);
        transaction.commit();
        lastFragment = fragment;
    }
}
