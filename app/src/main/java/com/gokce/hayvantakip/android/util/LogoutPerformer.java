package com.gokce.hayvantakip.android.util;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.ui.start.StartActivity;
import com.gokce.hayvantakip.api.util.SettingsStore;

import javax.inject.Inject;
import javax.inject.Singleton;

import java8.util.function.Consumer;

@Singleton
public class LogoutPerformer {
    private final Prefs prefs;
    private final SettingsStore tokenHelper;

    @Inject
    LogoutPerformer(@NonNull Prefs prefs,
                    @NonNull SettingsStore tokenHelper) {
        this.prefs = prefs;
        this.tokenHelper = tokenHelper;
    }

    public void perform(@Nullable Consumer<Void> onSuccess) {
        tokenHelper.clear();
        prefs.clear();
        if (onSuccess != null) {
            onSuccess.accept(null);
        }
    }

    public void performAndMoveStart(@NonNull Activity activity) {
        perform(v -> {
            activity.startActivity(new Intent(activity, StartActivity.class));
            activity.finishAffinity();
        });
    }

}
