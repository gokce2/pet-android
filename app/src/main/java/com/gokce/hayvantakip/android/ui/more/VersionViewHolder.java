package com.gokce.hayvantakip.android.ui.more;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gokce.hayvantakip.android.BuildConfig;
import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.databinding.ItemVersionBinding;


class VersionViewHolder extends RecyclerView.ViewHolder {
    private ItemVersionBinding binding;

    private VersionViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemVersionBinding.bind(itemView);
    }

    static VersionViewHolder create(@NonNull ViewGroup parent) {
        return new VersionViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_version, parent, false));
    }

    void bind() {
        binding.versionTextView.setText(binding.versionTextView.getContext()
                .getString(R.string.version_placeholder,
                        BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));
    }
}
