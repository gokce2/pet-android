package com.gokce.hayvantakip.android.ui.more;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.databinding.ItemMoreMenuBinding;


class MenuItemViewHolder extends RecyclerView.ViewHolder {
    private ItemMoreMenuBinding binding;

    private ItemClickListener listener;

    private MenuItemViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemMoreMenuBinding.bind(itemView);
        binding.menuLayout.setOnClickListener(v -> this.onClick());
        binding.toggleSwitch.setOnClickListener(v -> this.onClick());
    }

    static MenuItemViewHolder create(@NonNull ViewGroup parent) {
        return new MenuItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_more_menu, parent, false));
    }

    void bind(@NonNull MoreMenuItem item, ItemClickListener listener) {
        this.listener = listener;
        binding.tvItemSubtitle.setText(item.getSubtitle());
        binding.tvItemTitle.setText(item.getTitle());
        binding.ivItemIcon.setImageResource(item.getIcon());
        binding.ivNext.setVisibility(item.isHideNext() ? View.GONE : View.VISIBLE);
        binding.toggleSwitch.setVisibility(item.isToggle() ? View.VISIBLE : View.GONE);
        binding.toggleSwitch.setChecked(item.isChecked());
        binding.toggleSwitch.setEnabled(!item.isLoading());
        binding.progressBar.setVisibility(item.isLoading() ? View.VISIBLE : View.GONE);
    }

    private void onClick() {
        if (listener != null) {
            listener.onClick(getAdapterPosition());
        }
    }

    @FunctionalInterface
    public interface ItemClickListener {
        void onClick(int position);
    }

}
