package com.gokce.hayvantakip.android.ui.more;

import androidx.annotation.Nullable;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Builder
@Setter
@AllArgsConstructor
public class MoreMenuItem implements Serializable {
    private final ID      id;
    private       boolean hideNext;
    private       int     icon;
    private       int     title;
    private       int     subtitle;
    private       boolean toggle;
    private       boolean checked;
    private       boolean loading;


    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof MoreMenuItem) {
            return ((MoreMenuItem) obj).id.equals(id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int code = 12;
        if (id != null) {
            code += id.hashCode();
        }
        return code;
    }

    public enum ID implements Serializable {
        LOGOUT, ABOUT, HISTORY, MANAGE_ADDRESS, NOTIFICATIONS
    }

}
