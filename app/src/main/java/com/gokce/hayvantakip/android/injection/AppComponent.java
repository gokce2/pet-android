package com.gokce.hayvantakip.android.injection;

import com.gokce.hayvantakip.android.App;
import com.gokce.hayvantakip.android.ui.more.MoreModule;
import com.gokce.hayvantakip.api.ApiModule;
import com.gokce.hayvantakip.api.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Component(modules = {AppContextModule.class, AndroidInjectionModule.class,
        ActivityModule.class, FragmentModule.class, RetrofitModule.class,
        ApiModule.class, MoreModule.class, ServiceModule.class})
@Singleton
public interface AppComponent extends AndroidInjector<App> {
}
