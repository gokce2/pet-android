package com.gokce.hayvantakip.android.util.keyboard;

public interface KeyboardVisibilityEventListener {

    void onVisibilityChanged(boolean isOpen);
}
