package com.gokce.hayvantakip.android.injection;

import com.gokce.hayvantakip.android.service.DataUpdateService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    abstract DataUpdateService dataUpdateService();
}
