package com.gokce.hayvantakip.android.injection;


import com.gokce.hayvantakip.android.ui.DetailActivity;
import com.gokce.hayvantakip.android.ui.MainActivity;
import com.gokce.hayvantakip.android.ui.start.StartActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector
    abstract StartActivity startActivity();

    @ContributesAndroidInjector
    abstract DetailActivity detailActivity();
}
