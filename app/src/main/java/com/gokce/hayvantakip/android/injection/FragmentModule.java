package com.gokce.hayvantakip.android.injection;

import com.gokce.hayvantakip.android.ui.fragment.AboutFragment;
import com.gokce.hayvantakip.android.ui.map.MapFragment;
import com.gokce.hayvantakip.android.ui.more.MoreFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract MapFragment mapFragment();

    @ContributesAndroidInjector
    abstract AboutFragment aboutFragment();

    @ContributesAndroidInjector
    abstract MoreFragment moreFragment();
}
