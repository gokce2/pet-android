package com.gokce.hayvantakip.android.ui.start;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.base.BaseActivity;
import com.gokce.hayvantakip.android.databinding.ActivityStartBinding;
import com.gokce.hayvantakip.android.helper.Helper;
import com.gokce.hayvantakip.android.ui.MainActivity;
import com.gokce.hayvantakip.android.util.DialogUtil;
import com.gokce.hayvantakip.android.util.Installation;
import com.gokce.hayvantakip.android.util.Prefs;
import com.gokce.hayvantakip.api.TokenHelper;
import com.gokce.hayvantakip.api.UserService;
import com.gokce.hayvantakip.api.domain.User;
import com.gokce.hayvantakip.api.util.LoginRequest;
import com.gokce.hayvantakip.api.util.exception.ApiExceptionUtil;
import com.gokce.hayvantakip.api.util.exception.NoInternetException;
import com.gokce.hayvantakip.api.util.exception.NotFoundException;
import com.gokce.hayvantakip.api.util.exception.WrongPasswordException;

import java.io.File;
import java.io.FileOutputStream;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class StartActivity extends BaseActivity {
    private ActivityStartBinding binding;
    @Inject
    UserService userService;
    @Inject
    Prefs       prefs;
    @Inject
    TokenHelper tokenHelper;

    private Disposable disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        copyMap();
        if (prefs.getUser().isPresent()) {
            startActivityFinishingAffinity(MainActivity.class);
            return;
        }
        binding = ActivityStartBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnLogin.setOnClickListener(v -> {
            onLoginTap();
        });
    }

    private void onLoginTap() {
        //verify input values
        CharSequence username = binding.etUsername.getText();
        CharSequence password = binding.etPassword.getText();
        if (username == null || password == null) {
            DialogUtil.showOk(this, R.string.please_input_values);
            return;
        }
        String strUsername = username.toString().trim();
        String strPassword = password.toString().trim();
        if (TextUtils.isEmpty(strPassword) || TextUtils.isEmpty(strUsername)) {
            DialogUtil.showOk(this, R.string.please_input_values);
            return;
        }
        //server call to login user
        toggleProgress(true);
        disposable = userService.login(new LoginRequest(strUsername, strPassword, Installation.id(getApplicationContext())))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(error -> Single.error(ApiExceptionUtil.loginException(error)))
                .subscribe(u -> {
                    u.setPassword(strPassword);
                    onLoginSuccess(u);
                }, this::handleError);
    }

    private void toggleProgress(boolean show) {
        binding.btnLogin.toggleProgress(show);
        binding.btnRememberMe.setEnabled(!show);
        binding.etPassword.setEnabled(!show);
        binding.etUsername.setEnabled(!show);
    }

    private void copyMap() {
        Observable.create(emitter -> {
            try {
                File cacheDir = new File(getCacheDir(), "tiles/");
                if (cacheDir.mkdirs()) {
                    File cacheFile = new File(cacheDir, "cache.db");
                    if (cacheFile.exists()) {
                        return;
                    }
                    if (cacheFile.createNewFile()) {
                        Helper.copyFile(getAssets().open("cache.db"),
                                new FileOutputStream(cacheFile));
                    }
                }
            } catch (Exception e) {
                Log.e("copyFile", "copyMap: failed to copy file" + e.getLocalizedMessage());
            }
        })
                .subscribeOn(Schedulers.io())
                .subscribe();
    }
    private void onLoginSuccess(@NonNull User user) {
        toggleProgress(false);
        prefs.saveUser(user);
        tokenHelper.afterLogin(user);
        startActivityFinishingAffinity(MainActivity.class);
    }

    private void handleError(@NonNull Throwable error) {
        toggleProgress(false);
        if (error instanceof WrongPasswordException) {
            DialogUtil.showOk(this, R.string.please_valid_password);
        } else if (error instanceof NotFoundException) {
            DialogUtil.showOk(this, R.string.wrong_username);
        } else if (error instanceof NoInternetException) {
            DialogUtil.showOk(this, R.string.no_internet);
        } else {
            DialogUtil.showOk(this, error.getLocalizedMessage());
        }
    }

    @Override
    protected void onDestroy() {
        if (disposable != null) {
            disposable.dispose();
        }
        super.onDestroy();
    }
}
