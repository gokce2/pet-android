package com.gokce.hayvantakip.android.ui.more;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.gokce.hayvantakip.android.R;
import com.gokce.hayvantakip.android.base.BaseViewHolder;
import com.gokce.hayvantakip.android.databinding.ItemProfileBinding;
import com.gokce.hayvantakip.api.domain.User;


class ProfileViewHolder extends BaseViewHolder<User> {
    private ItemProfileBinding binding;


    private ProfileViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemProfileBinding.bind(itemView);
    }

    static ProfileViewHolder create(@NonNull ViewGroup parent) {
        return new ProfileViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_profile, parent, false));
    }

    public void bind(@NonNull User user) {
        binding.nameTextView.setText(user.getName());
        binding.usernameTextView.setText(user.getUsername());
    }
}
