package com.gokce.hayvantakip.android.ui.more;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gokce.hayvantakip.android.util.Prefs;
import com.gokce.hayvantakip.api.domain.User;

import java.util.List;

import javax.inject.Inject;

public class MoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int                PROFILE_VIEW_TYPE = 0xee;
    private static final int                MENU_VIEW_TYPE    = 0xaa;
    private static final int                VERSION_VIEW_TYPE = 0xdd;
    private final        List<MoreMenuItem> menuItems;
    private final        Prefs              prefs;
    private              OnMenuItemClick    listener;

    @Inject
    MoreAdapter(@NonNull Prefs prefs,
                @NonNull List<MoreMenuItem> menuItems) {
        this.menuItems = menuItems;
        this.prefs     = prefs;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VERSION_VIEW_TYPE) {
            return VersionViewHolder.create(parent);
        }
        return viewType == PROFILE_VIEW_TYPE ? ProfileViewHolder.create(parent) :
                MenuItemViewHolder.create(parent);
    }

    public void setListener(@NonNull OnMenuItemClick listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position > menuItems.size()) {
            return VERSION_VIEW_TYPE;
        }
        return position == 0 ? PROFILE_VIEW_TYPE : MENU_VIEW_TYPE;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProfileViewHolder) {
            ((ProfileViewHolder) holder).bind(prefs.getUser().orElse(new User()));
        } else if (holder instanceof MenuItemViewHolder) {
            ((MenuItemViewHolder) holder).bind(menuItems.get(position - 1), this::onClick);
        } else if (holder instanceof VersionViewHolder) {
            ((VersionViewHolder) holder).bind();
        }
    }

    private void onClick(int position) {
        if (listener != null) {
            if (position == 0) {
                return;
            }
            position = position - 1;
            if (position < menuItems.size()) {
                MoreMenuItem item = menuItems.get(position);
                listener.onClick(item);
            }
        }
    }

    @Override
    public int getItemCount() {
        return menuItems.size() + 2;
    }


    interface OnMenuItemClick {
        void onClick(@NonNull MoreMenuItem item);
    }
}
