package com.gokce.hayvantakip.android.ui.more;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gokce.hayvantakip.android.base.BaseFragment;
import com.gokce.hayvantakip.android.util.LogoutPerformer;

import javax.inject.Inject;

public class MoreFragment extends BaseFragment {
    private static final String TAG = MoreFragment.class.getSimpleName();
    @Inject
    MoreAdapter     accountAdapter;
    @Inject
    LogoutPerformer logoutPerformer;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        recyclerView = new RecyclerView(context);
        loadData();
        return recyclerView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void loadData() {
        MenuItemClickHandler clickHandler = new MenuItemClickHandler(this);
        accountAdapter.setListener(clickHandler);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(accountAdapter);
        recyclerView.setVisibility(View.VISIBLE);
    }

    void logout() {
        activity().ifPresent(a -> {
            logoutPerformer.performAndMoveStart(a);
        });
    }

}
